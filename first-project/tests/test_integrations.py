import os
import pytest
from app import app
from flask import url_for


@pytest.fixture
def client():
    app.config['TESTING'] = True
    app.config['SERVER_NAME'] = 'TEST'
    client = app.test_client()
    with app.app_context():
        pass
    app.app_context().push()
    yield client


def test_index(client):
    rv = client.get('/')
    assert rv.status_code == 200
    assert b'Index' in rv.data


def test_hello(client):
    rv = client.get('/hello/')
    assert rv.status_code == 200
    assert b'Hello World !' in rv.data

    rv = client.get('/hello/test')
    assert rv.status_code == 200
    assert b'Hello test !' in rv.data

    rv = client.get('/hello/12@')
    assert rv.status_code == 200
    assert b'Hello 12@ !' in rv.data
