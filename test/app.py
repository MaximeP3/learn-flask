from flask import Flask
app = Flask(__name__)


@app.route('/')
def hello_worl():
    return 'Index Page'


@app.route('/hello')
def hello():
    return 'Hello World !'


@app.route('/say_hello/<name>')
def say_hello(name):
    return "Hello %s" % name


@app.route('/add/<int:facteur_gauche>/<int:facteur_droite>')
def add(facteur_gauche, facteur_droite):
    return str(facteur_gauche + facteur_droite)


@app.route('/minus/<int:facteur_gauche>/<int:facteur_droite>')
def minus(facteur_gauche, facteur_droite):
    return str(facteur_gauche - facteur_droite)


@app.route('/multiply/<int:facteur_gauche>/<int:facteur_droite>')
def multiply(facteur_gauche, facteur_droite):
    return str(facteur_gauche * facteur_droite)


@app.route('/divide/<int:facteur_gauche>/<int:facteur_droite>')
def divide(facteur_gauche, facteur_droite):
    return 'Err : Div by 0' if facteur_droite == 0 else str(facteur_gauche / facteur_droite)
