import requests


def getAll():
    resp = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')
    if resp.status_code != 200:
        raise NameError('GET /dinosaurs/ Err {}'.format(resp.status_code))

    res = []
    for todo_item in resp.json():
        res.append(todo_item)

    return res


def getBySlug(slug):
    resp = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/%s' % slug)
    if resp.status_code != 200:
        raise NameError('GET /dinosaur/{}/ Err {}'.format(slug, resp.status_code))

    return resp.json()
