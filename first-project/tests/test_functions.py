import pytest

import maths

def test_add():
    assert 4 == maths.add(2, 2)
    assert 10 == maths.add(3, 7)
    assert 10 == maths.add(7, 3)

def test_multiplication():
    assert 4 == maths.multiplication(2, 2)
    assert 21 == maths.multiplication(3, 7)
    assert 21 == maths.multiplication(7, 3)


def test_minus():
    assert 0 == maths.minus(2, 2)
    assert -4 == maths.minus(3, 7)
    assert 4 == maths.minus(7, 3)


def test_divide():
    assert 1 == maths.divide(2, 2)
    assert 3/7 == maths.divide(3, 7)
    assert 7/3 == maths.divide(7, 3)
    assert None == maths.divide(1, 0)
