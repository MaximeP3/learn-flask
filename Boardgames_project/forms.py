from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, HiddenField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, Length

class BoardgameForm(FlaskForm):
    name = StringField('Name', validators=[
                       DataRequired(), Length(min=3, max=20)])
    slug = HiddenField('Slug')
    description = StringField('Description', validators=[
                       DataRequired()])
    release_date = DateField('Release date', validators=[DataRequired()])
    category = SelectField('Category', coerce=int)
