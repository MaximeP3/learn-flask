from flask import Flask, render_template
app = Flask(__name__)

import api
import random

@app.route('/')
def index():
    dinosaurs = api.getAll()
    return render_template('index.html', len = len(dinosaurs), dinosaurs = dinosaurs)


@app.route('/dinosaur/<slug>')
def dinosaur(slug):
    try:
        dinosaur = api.getBySlug(slug)
        dinosaurs = random.sample(api.getAll(), 3)
    except:
        return render_template('404.html')

    dinosaur['description'] = api.htmlStrFormat(dinosaur['description'])

    return render_template('dinosaur.html', dinosaur = dinosaur, dinosaurs = dinosaurs)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404
