from flask import Flask, render_template, flash, redirect, url_for
app = Flask(__name__)

import click
import models
import forms
from slugify import slugify

import os
SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY

@app.route('/')
def index():
    return render_template('index.html')


@app.route('/games')
def games():
    return render_template('games.html', query = models.Boardgame.select())


@app.route('/game/<slug>')
def game(slug):
    return render_template('game.html', query = models.Boardgame.select().where(models.Boardgame.slug == slug)[0])


@app.route('/categories')
def categories():
    return render_template('categories.html', query = models.Category.select())


@app.route('/category/<slug>')
def category(slug):
    return render_template('games.html',
        query = models.Boardgame.select()
                                .join(models.Category)
                                .where(models.Category.slug == slug))


@app.route('/boardgame/create', methods=['GET', 'POST', ])
def boardgame_create():
    boardgame = models.Boardgame()
    form = forms.BoardgameForm()
    form.category.choices = [(m.id, m.name) for m in models.Category.select()]

    if form.validate_on_submit():
        form.slug.data = slugify(form.data['name'],to_lower=True)
        if len(models.Boardgame.select().where(models.Boardgame.slug == form.slug.data)) > 0:
            flash('Error during boardgame creation : Similar name already in database.')
            return render_template('form.html', form=form)
        form.populate_obj(boardgame)
        boardgame.save()
        flash('Boardgame created !')
        return redirect(url_for('games'))

    return render_template('form.html', form=form)


@app.cli.command()
def initdb():
    """Create database"""
    models.create_tables()
    click.echo('Initialized the database')


@app.cli.command()
def dropdb():
    """Drop database tables"""
    models.drop_tables()
    click.echo('Dropped tables from database')


@app.cli.command()
def fakedata():
    from faker import Faker
    fake = Faker()

    for pk in range(0, 5):
        name = fake.company()
        models.Category.create(name=name,
                               slug=slugify(name, to_lower=True))

    for category in models.Category.select():
        for pk in range(0, 4):
            name = fake.company()
            models.Boardgame.create(name=name,
                                    slug=slugify(name, to_lower=True),
                                    description=fake.catch_phrase(),
                                    release_date=fake.date(),
                                    category=category)
