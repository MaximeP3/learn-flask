import os
import pytest
from app import app
from flask import url_for


@pytest.fixture
def client():
    app.config['TESTING'] = True
    app.config['SERVER_NAME'] = 'TEST'
    client = app.test_client()
    with app.app_context():
        pass
    app.app_context().push()
    yield client


def test_index(client):
    rv = client.get('/')
    assert rv.status_code == 200
    assert b'THE WORLD OF JURASSIC PARK' in rv.data


def test_hello(client):
    rv = client.get('/dinosaur/dilophosaurus')
    assert rv.status_code == 200
    assert b'Discover the Dilophosaurus' in rv.data

    rv = client.get('/dinosaur/parasaurolophus')
    assert rv.status_code == 200
    assert b'Discover the Parasaurolophus' in rv.data

def test_notfound(client):
    rv = client.get('/dinosaur')
    assert rv.status_code == 404
    assert b'Page not found' in rv.data

    rv = client.get('/dosdfkopdsfk')
    assert rv.status_code == 404
    assert b'Page not found' in rv.data
