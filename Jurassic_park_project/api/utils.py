def htmlStrFormat(str):
    for i in range(str.count('**')):
        if i % 2 == 0:
            str = str.replace('**', '<b>', 1)
        else:
            str = str.replace('**', '</b>', 1)

    for i in range(str.count('*')):
        if i % 2 == 0:
            str = str.replace('*', '<strong>', 1)
        else:
            str = str.replace('*', '</strong>', 1)

    return str
