from peewee import *
import datetime

database = SqliteDatabase("boardgames.sqlite3")


class BaseModel(Model):

    class Meta:
        database = database


class Category(BaseModel):
    name = CharField()
    slug = CharField()
    created_at = DateTimeField(default=datetime.datetime.now)


class Boardgame(BaseModel):
    name = CharField()
    slug = CharField()
    description = TextField()
    release_date = DateField()
    created_at = DateTimeField(default=datetime.datetime.now)
    category = ForeignKeyField(Category, backref="boardgames")


def create_tables():
    with database:
        database.create_tables([Boardgame, Category, ])


def drop_tables():
    with database:
        database.drop_tables([Boardgame, Category, ])
