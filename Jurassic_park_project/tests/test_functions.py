import pytest

import api

def test_htmlStrFormat():
    assert '<b>test</b>' == api.htmlStrFormat('**test**')
    assert '<strong>test</strong>' == api.htmlStrFormat('*test*')


def test_api_getAll():
    try:
        api.getAll()[0]['avatar']
        api.getAll()[0]['name']
        api.getAll()[0]['slug']
        api.getAll()[0]['uri']
        res = True
    except KeyError:
        res = False

    assert res == True


def test_api_getBySlug():
    dinosaur = api.getBySlug(api.getAll()[0]['slug'])
    try:
        dinosaur['avatar']
        dinosaur['description']
        dinosaur['diet']
        dinosaur['height']
        dinosaur['length']
        dinosaur['name']
        dinosaur['name_meaning']
        dinosaur['slug']
        dinosaur['uri']
        dinosaur['weight']
        res = True
    except KeyError:
        res = False

    assert res == True
