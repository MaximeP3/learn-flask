def multiplication(left_factor, right_factor):
    return left_factor * right_factor


def add(left_factor, right_factor):
    return left_factor + right_factor


def minus(left_factor, right_factor):
    return left_factor - right_factor


def divide(left_factor, right_factor):
    return None if right_factor == 0 else left_factor / right_factor
